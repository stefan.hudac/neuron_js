const path = require("path");

const modifiers = require("./src/Modifiers");
const neuron = require("./src/Neuron");

//const template = path.join(process.cwd(), "tests", "templates", "aa.md");
const template = "./tests/templates/aa.md";

let data = {};

data.modifiers = Object.keys(modifiers).map((item) => {
  return { name: item };
});

// console.log(data);
// console.log(template);

let n = new neuron.Neuron(data);
console.log(n.parse_file(template));
