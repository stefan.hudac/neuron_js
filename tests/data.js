module.exports = {
  author: {
    first_name: "Stefan",
    last_name: "Hudac",
  },
  title: "Neuron is the best template utility in the known universe.",
  summary:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  numbers: {
    integer: 42,
    decimal: 9.75,
    zero: 0,
  },
  logical: {
    true: true,
    false: false,
  },
  people: [
    {
      first_name: "Luke",
      last_name: "Skywalker",
      occupation: "Jedi Knight",
      gender: "male",
    },
    {
      first_name: "Lea",
      last_name: "Organa",
      occupation: "Princess",
      gender: "female",
    },
    {
      first_name: "Han",
      last_name: "Solo",
      occupation: "Smuggler",
      gender: "male",
    },
    {
      first_name: "Obi-Wan",
      last_name: "Kenobi",
      occupation: "Jedi Master",
      gender: "male",
    },
    {
      first_name: "Anakin",
      last_name: "Skywalker",
      occupation: "Dark Lord of the Sith",
      gender: "male",
    },
  ],
};
