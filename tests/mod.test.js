const fs = require("fs");
const path = require("path");

const data = require("./data");
const neuron = require("../src/Neuron");
const modifiers = require("../src/Modifiers");

const tests_dir = "./tests/templates/modifiers";
const results_dir = "./tests/results/modifiers";

Object.keys(modifiers)
  .filter((modifier) => modifiers[modifier] instanceof Function)
  .forEach((modifier) => {
    let name = modifier.substr(4).toLowerCase();
    // console.log(name);
    it(`Test modifier: ${name}`, () => {
      let n = new neuron.Neuron(data);
      let t = n.render_file(path.join(tests_dir, `${name}.md`));

      let p = path.join(results_dir, `${name}.md`);
      let r = fs.existsSync(p) ? fs.readFileSync(p).toString() : null;

      expect(t).toBe(r);
    });
  });
