# Modifiers

Provide a way to change or modify an output after a variable was resolved.

## capitalize

Capitalizes first character of every word.

    String input: Neuron Is The Best Template Utility In The Known Universe.
    Numerical input: 42
    Numerical 0 input: 0
    Logical true input: True
    Logical false input: False
    Object input: [Object Object]
    Array input: [Object Object],[object Object],[object Object],[object Object],[object Object]
    Missing input: 

## cat

Joins all following parameters together with variable. Parameters may point to a key in data. If initial key does not return a value, 'cat' returns nothing.

    String input: Neuron is the best template utility in the known universe.JohnDoeStefanHudacNeuron is the best template utility in the known universe.429.75
    Numerical input: 42JohnStefan42
    Numerical 0 input: 0JohnStefan42
    Object input: [object Object]JohnStefan42
    Array input: [object Object],[object Object],[object Object],[object Object],[object Object]JohnStefan42
    Missing input: JohnStefan42

    Object parameter: Neuron is the best template utility in the known universe.JohnDoe[object Object]42
    Array parameter: Neuron is the best template utility in the known universe.JohnDoe[object Object],[object Object],[object Object],[object Object],[object Object]42

## count_paragraphs

    Neuron is the best template utility in the known universe.

## count_sentences

Counts occurrences of end sentence characters (.?!).

    Input is a text paragraph: 0
    String input 1: 1
    Numerical input: 0
    Object input: 0
    Array input: 0
    Missing input: 0

## count_words

    Neuron is the best template utility in the known universe.

## date_format

    Neuron is the best template utility in the known universe.

## default

Default can point to a key in data and can fallback even several times to another set value.

    And the winner is: Neuron is the best template utility in the known universe.
    Numerical default: TBD
    Object default: [object Object]
    Array default: [object Object],[object Object],[object Object],[object Object],[object Object]
    Missing default: TBD

## escape

    Neuron is the best template utility in the known universe.

## indent

    Neuron is the best template utility in the known universe.

## length

Provides length of a string or an array.

    Length of "Neuron is the best template utility in the known universe." is 58
    Numeric input: 
    Object input: 
    Array input: Our records have 5 people.
    Missing input: 

## lower

Converts string to lowercase letters.

    String input: neuron is the best template utility in the known universe.
    Numerical input: 
    Array input: 
    Object input: 
    Missing input: Numerical input: 

## nl2br

    Neuron is the best template utility in the known universe.

## regex_replace

    Neuron is the best template utility in the known universe.

## replace

    Neuron is the best template utility in the known universe.

## spacify

    Neuron is the best template utility in the known universe.

## string_format

    Neuron is the best template utility in the known universe.

## strip

    Neuron is the best template utility in the known universe.

## strip_tags

    Neuron is the best template utility in the known universe.

## substring

    Neuron is the best template utility in the known universe.

## truncate

    Neuron is the best template utility in the known universe.

## upper

Converts string to uppercase letters.

    String input: NEURON IS THE BEST TEMPLATE UTILITY IN THE KNOWN UNIVERSE.
    Numerical input: 
    Array input: 
    Object input: 
    Missing input: Numerical input: 

## wordwrap

    Neuron is the best template utility in the known universe.
