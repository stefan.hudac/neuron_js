# Modifiers

{foreach $modifier in $modifiers}

## {$modifier.name|substring:4|lower}

{$modifier.description|default:No description available}

Example:

```
{include file=$modifier.name|substring:4|lower|sufix:.md|prefix:tests/templates/modifiers/ process=no}
```

Results:

```
{include file=$modifier.name|substring:4|lower|sufix:.md|prefix:tests/results/modifiers/ process=no}
```

{foreachelse}

Nothing here

{/foreach}
