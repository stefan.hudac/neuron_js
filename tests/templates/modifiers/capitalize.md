String input: {$title|capitalize}
Numerical input: {$numbers.integer|capitalize}
Numerical 0 input: {$numbers.zero|capitalize}
Logical true input: {$logical.true|capitalize}
Logical false input: {$logical.false|capitalize}
Object input: {$author|capitalize}
Array input: {$people|capitalize}
Missing input: {$missing|capitalize}
