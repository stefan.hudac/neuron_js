String input: {$title|lower}
Numerical input: {$numbers.integer|lower}
Array input: {$people|lower}
Object input: {$author|lower}
Missing input: Numerical input: {$missing|lower}
