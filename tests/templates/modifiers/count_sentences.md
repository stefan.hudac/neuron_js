Paragraph input: {$paragraph|count_sentences}
String input: {$title|count_sentences}
Numerical input: {$numbers.integer|count_sentences}
Object input: {$author|count_sentences}
Array input: {$people|count_sentences}
Missing input: {$missing|count_sentences}
