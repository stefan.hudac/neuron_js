String input: {$title|cat:John:Doe:$author.first_name:$author.last_name:$missing:$title:$numbers.integer:$numbers.decimal}
Numerical input: {$numbers.integer|cat:John:$author.first_name:$numbers.integer}
Numerical 0 input: {$numbers.zero|cat:John:$author.first_name:$numbers.integer}
Object input: {$author|cat:John:$author.first_name:$numbers.integer}
Array input: {$people|cat:John:$author.first_name:$numbers.integer}
Missing input: {$missing|cat:John:$author.first_name:$numbers.integer}

Object parameter: {$title|cat:John:Doe:$author:$numbers.integer}
Array parameter: {$title|cat:John:Doe:$people:$numbers.integer}
