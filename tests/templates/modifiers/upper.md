String input: {$title|upper}
Numerical input: {$numbers.integer|upper}
Array input: {$people|upper}
Object input: {$author|upper}
Missing input: Numerical input: {$missing|upper}
