Length of "{$title}" is {$title|length}
Numeric input: {$numbers.integer|length}
Object input: {$author|length}
Array input: Our records have {$people|length} people.
Missing input: {$missing|length}
