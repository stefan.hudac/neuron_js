# Modifiers

Provide a way to change or modify an output after a variable was resolved.

## capitalize

Capitalizes first character of every word.

    String input: {$title|capitalize}
    Numerical input: {$numbers.integer|capitalize}
    Numerical 0 input: {$numbers.zero|capitalize}
    Logical true input: {$logical.true|capitalize}
    Logical false input: {$logical.false|capitalize}
    Object input: {$author|capitalize}
    Array input: {$people|capitalize}
    Missing input: {$missing|capitalize}

## cat

Joins all following parameters together with variable. Parameters may point to a key in data. If initial key does not return a value, 'cat' returns nothing.

    String input: {$title|cat:John:Doe:$author.first_name:$author.last_name:$missing:$title:$numbers.integer:$numbers.decimal}
    Numerical input: {$numbers.integer|cat:John:$author.first_name:$numbers.integer}
    Numerical 0 input: {$numbers.zero|cat:John:$author.first_name:$numbers.integer}
    Object input: {$author|cat:John:$author.first_name:$numbers.integer}
    Array input: {$people|cat:John:$author.first_name:$numbers.integer}
    Missing input: {$missing|cat:John:$author.first_name:$numbers.integer}

    Object parameter: {$title|cat:John:Doe:$author:$numbers.integer}
    Array parameter: {$title|cat:John:Doe:$people:$numbers.integer}

## count_paragraphs

    {$title|count_paragraphs}

## count_sentences

Counts occurrences of end sentence characters (.?!).

    Input is a text paragraph: {$paragraph|count_sentences}
    String input 1: {$title|count_sentences}
    Numerical input: {$numbers.integer|count_sentences}
    Object input: {$author|count_sentences}
    Array input: {$people|count_sentences}
    Missing input: {$missing|count_sentences}

## count_words

    {$title|count_words}

## date_format

    {$title|date_format}

## default

Default can point to a key in data and can fallback even several times to another set value.

    And the winner is: {$missing|default:$title:TBD}
    Numerical default: {$missing|default:$missing:TBD}
    Object default: {$missing|default:$author:TBD}
    Array default: {$missing|default:$people:TBD}
    Missing default: {$missing|default:$missing:TBD}

## escape

    {$title|escape}

## indent

    {$title|indent}

## length

Provides length of a string or an array.

    Length of "{$title}" is {$title|length}
    Numeric input: {$numbers.integer|length}
    Object input: {$author|length}
    Array input: Our records have {$people|length} people.
    Missing input: {$missing|length}

## lower

Converts string to lowercase letters.

    String input: {$title|lower}
    Numerical input: {$numbers.integer|lower}
    Array input: {$people|lower}
    Object input: {$author|lower}
    Missing input: Numerical input: {$missing|lower}

## nl2br

    {$title|nl2br}

## regex_replace

    {$title|regex_replace}

## replace

    {$title|replace}

## spacify

    {$title|spacify}

## string_format

    {$title|string_format}

## strip

    {$title|strip}

## strip_tags

    {$title|strip_tags}

## substring

    {$title|substring}

## truncate

    {$title|truncate}

## upper

Converts string to uppercase letters.

    String input: {$title|upper}
    Numerical input: {$numbers.integer|upper}
    Array input: {$people|upper}
    Object input: {$author|upper}
    Missing input: Numerical input: {$missing|upper}

## wordwrap

    {$title|wordwrap}
