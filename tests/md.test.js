const fs = require("fs");
const path = require("path");

const data = require("./data");
const neuron = require("../src/Neuron");

const tests_dir = "./tests/templates";
const results_dir = "./tests/results";

const mds = fs.readdirSync(tests_dir).filter((item) => item.endsWith(".md"));

mds.forEach((md) =>
  it(`Test file: ${path.join(tests_dir, md)}`, () => {
    let n = new neuron.Neuron(data);
    let t = n.render_file(path.join(tests_dir, md));
    let p = path.join(results_dir, md);
    let r = fs.existsSync(p) ? fs.readFileSync(p).toString() : null;
    expect(t).toBe(r);
  })
);

// const modifiers = require("../src/Modifiers");
// Object.keys(modifiers)
//   .filter((modifier) => modifier instanceof Function)
//   .forEach((modifier) => {
//     it(`Test modifier: ${md}`, () => {
//       let n = new neuron.Neuron(data);
//       let t = n.parse_file(
//         path.join(tests_dir, "modifiers", `${modifier}.txt`)
//       );

//       let p = path.join(results_dir, "modifiers", `${modifier}.txt`);
//       let r = fs.existsSync(p) ? fs.readFileSync(p).toString() : null;

//       expect(t).toBe(r);
//     });
//   });
