const neuron = require("./Neuron");
const Modifiers = require("./Modifiers").Modifiers;
const FileSystem = require("./FileSystem").FileSystem;

module.exports = {
  Neuron: neuron.Neuron,
  render_file: neuron.render_file,
  render: neuron.render,
  Modifiers,
  FileSystem,
};
