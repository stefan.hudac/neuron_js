const path = require("path");
const fs = require("fs");

class FileSystem {
  constructor(root = null) {
    // this.root = root && typeof root == "string" ? root : process.cwd();
    this.root = root;
  }
  read(filename) {
    if (!this.root) {
      this.root = path.dirname(filename);
    }
    filename = path.isAbsolute(filename)
      ? filename
      : path.join(this.root, filename);
    return fs.readFileSync(filename).toString();
  }
}

module.exports = {
  FileSystem,
};
