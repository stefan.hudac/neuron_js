module.exports = {
  mod_CAPITALIZE(text, params = null) {
    text = text.toString();
    return text.replace(/\w\S*/g, (txt) => {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  },
  mod_CAT(text, params = null) {
    text = text.toString();

    return params && params instanceof Array
      ? `${text}${params.join("")}`
      : text;
  },
  mod_COUNT_PARAGRAPHS(text, params = null) {
    if (!(text instanceof String || typeof text === "string")) return "";
    return text;
  },
  mod_COUNT_SENTENCES(text, params = null) {
    text = text.toString();
    return (text.match(/[.?!]/g) || []).length;
  },
  mod_COUNT_WORDS(text, params = null) {
    return text;
  },
  mod_DATE_FORMAT(text, params = null) {
    return text;
  },
  mod_DEFAULT(text, params = null) {
    text = text.toString();
    return (
      text ||
      (params && Array.isArray(params) && params.find((item) => item)) ||
      ""
    );
  },
  mod_ESCAPE(text, params = null) {
    return text;
  },
  mod_INDENT(text, params = null) {
    return text;
  },
  mod_LENGTH(obj, params = null) {
    return obj &&
      obj.hasOwnProperty("length") &&
      !(obj.length instanceof Function)
      ? obj.length
      : "";
  },
  mod_LOWER(text, params = null) {
    return text instanceof String || typeof text === "string"
      ? text.toLowerCase()
      : "";
  },
  mod_NL2BR(text, params = null) {
    return text;
  },
  mod_PREFIX(text, params) {
    return `${params[0] || ""}${text}`;
  },
  mod_REGEX_REPLACE(text, params = null) {
    return text;
  },
  mod_REPLACE(text, params = null) {
    return text;
  },
  mod_SPACIFY(text, params = null) {
    return text;
  },
  mod_STRING_FORMAT(text, params = null) {
    return text;
  },
  mod_STRIP(text, params = null) {
    return text;
  },
  mod_STRIP_TAGS(text, params = null) {
    return text;
  },
  mod_SUFIX(text, params) {
    return `${text}${params[0] || ""}`;
  },
  mod_SUBSTRING(text, params = null) {
    text = text.toString();
    params = params.map((item) => parseInt(item));
    params = params.length > 2 ? params.slice(2) : params;
    switch (params.length) {
      case 2:
        return text.substr(params[0], params[1]);
      case 1:
        return text.substr(params[0]);
      default:
        return text;
    }
  },
  mod_TRUNCATE(text, params = null) {
    return text;
  },
  mod_UPPER(text, params = null) {
    return text instanceof String || typeof text === "string"
      ? text.toUpperCase()
      : "";
  },
  mod_WORDWRAP(text, params = null) {
    return text;
  },
};
