const FileSystem = require("./FileSystem");
const modifiers = require("./Modifiers");

class Neuron {
  constructor(vm = {}, fs = null, tc = null, tm = null) {
    // Variable manager
    this.vm = vm;
    // File system
    this.fs = fs || new FileSystem.FileSystem();
    // Token cache
    this.tc = tm || {};
    // Text modifiers
    this.tm = tm || modifiers;

    this.loop = {};
    this.slots = {};
    this.commands = {
      LDELIM: 0,
      RDELIM: 0,
      INCLUDE: 0,
      DEFINESLOT: 0,
      FILLSLOT: 1,
      ENDFILLSLOT: -1,
      IF: 1,
      ELSEIF: 0,
      ELSE: 0,
      ENDIF: -1,
      FOREACH: 1,
      FOREACHELSE: 0,
      ENDFOREACH: -1,
      STRIP: 1,
      ENDSTRIP: -1,
      LITERAL: 1,
      ENDLITERAL: -1,
      VAR: 0,
      TXT: 0,
      SET: 0,
    };
  }

  render_file(filename) {
    let tokens = [];
    if (this.tc[filename]) {
      tokens = this.tc[filename];
    } else {
      tokens = this.tokenize(this.fs.read(filename));
      this.tc[filename] = tokens;
    }
    return this.parse_tokens(tokens);
  }

  render_string(text) {
    tokens = this.tokenize(this.fs.read(text));
    return this.parse_tokens(tokens);
  }

  parse_tokens(tokens) {
    // console.log("Tokens to process:", tokens);
    let reading_block = false;
    let result = [];
    let block = [];
    let block_level = 0;
    let block_cmd = "";

    tokens.forEach((token) => {
      if (reading_block) {
        // console.log("token:", token);
        block.push(token);
        if (token[2] < block_level) {
          // console.log("asdf");
          reading_block = false;
          let method_name = `cmd_${block_cmd}`;
          if (this[method_name]) {
            result.push(this[method_name](block));
            block = [];
          }
        }
      } else {
        let level = this.commands[token[0]];
        if (level === 1) {
          reading_block = true;
          block_cmd = token[0];
          block_level = token[2];
          block.push(token);
        } else {
          let method_name = `cmd_${token[0]}`;
          if (this[method_name]) {
            result.push(this[method_name](token[1]));
          }
        }
      }
    });
    //console.log('Result:', result, result.length);
    return result.join("");
  }

  parse_variable_expression(text) {
    let out = [];
    let chunks = `|${text}|`
      .split(/([|:](?=(?:[^"]*"[^"]*")*(?![^"]*")))/)
      .map((item) => item.trim());
    let modifier = {};
    chunks.forEach((chunk) => {
      switch (chunk) {
        case "|":
          if (modifier && modifier.modifier) {
            out.push(modifier);
            modifier = {};
          }
          break;
        case ":":
          break;
        default:
          if (modifier.modifier) {
            modifier.params.push(chunk);
          } else {
            modifier.modifier = chunk;
            modifier.params = [];
          }
      }
    });
    if (out.length > 0) {
      return {
        name: out[0].modifier,
        modifiers: out.length > 1 ? out.slice(1) : [],
      };
    }
    return {};
  }

  tokenize(text) {
    let chunks = text.split(/(\{[^\{]+\})/);
    // console.log("Tokens", tokens);
    let tokens = [];
    let level = 0;
    let is_literal = false;
    let is_comment = false;
    chunks.forEach((chunk) => {
      let token = ["TXT", chunk];
      // console.log(token);
      if (is_literal) {
        if (chunk === "{/literal}") {
          is_literal = false;
          return;
        }
      } else if (is_comment) {
        if (chunk.slice(-2) === "*}") {
          is_comment = false;
        }
        return;
      } else {
        // console.log("Chunk:", chunk);
        if (chunk.startsWith("{*")) {
          is_comment = true;
          return;
        } else if (chunk.startsWith("{$")) {
          token = ["VAR", chunk.substring(2, chunk.length - 1)];
        } else if (chunk.startsWith("{/")) {
          let key = `end${chunk.substring(2, chunk.length - 1)}`
            .trim()
            .toUpperCase();
          // console.log("key:", key);
          if (Object.keys(this.commands).includes(key)) {
            // console.log("asdfasfasdfadfadfasdfasdfa");
            token = [key, null];
            // console.log(token);
          }
        } else if (chunk.startsWith("{")) {
          // console.log('T:', chunk);
          let key = chunk.substring(1, chunk.length - 1);
          if (key.toUpperCase() === "LITERAL") {
            is_literal = true;
            return;
          }
          let param = null;
          let pos = key.indexOf(" ");
          if (pos !== -1) {
            param = key.substring(pos + 1, key.length);
            key = key.substring(0, pos);
          }
          key = key.toUpperCase();
          // console.log(`>${key}<`);
          if (Object.keys(this.commands).includes(key)) {
            token = [key, param];
          }
        }
      }
      let indent = this.commands[token[0]] || 0;
      // console.log("indent to set:", indent);
      level += indent;
      tokens.push([token[0], token[1], level]);
    });
    return tokens;
  }

  get_keys(text, defaults) {
    let out = { ...defaults };
    text.split(" ").forEach((item) => {
      let chunks = item.split("=").map((chunk) => chunk.trim());
      if (chunks[0] && chunks[0] in out && chunks[1]) {
        out[chunks[0]] = chunks[1].startsWith("$")
          ? this.get_var_value(chunks[1].substr(1))
          : chunks[1];
      }
    });
    return out;
  }

  set_var_value(name, value) {
    let chunks = name.split(".");
    let cur = this.vm;
    for (let i = 0; i < chunks.length; i++) {
      if (cur[chunks[i]]) {
        cur = cur[chunks[i]];
      } else if (i + 1 === chunks.length) {
        cur[chunks[i]] = value;
      } else {
        cur[chunks[i]] = {};
      }
    }
  }

  get_var_value(text) {
    let variable = this.parse_variable_expression(text);
    let varname = variable.name;
    // console.log("Looking for variable:", varname);
    let varnames = varname.split(".");

    let varvalue;
    if (varnames[0] in this.loop) {
      // console.log("aaa:", this.loop[varnames[0]]);
      // console.log("varnames", varnames);
      // console.log("looking for:", varnames[1]);

      let pos = this.loop[varnames[0]].pos;
      switch (varnames[1]) {
        case "INDEX":
          varvalue = pos + 1;
          break;
        case "FIRST":
          varvalue = pos === 0;
          break;
        case "LAST":
          varvalue = pos === this.loop[varnames[0]].loop.length;
          break;
        case "EVEN":
          varvalue = pos % 2 === 0;
          break;
        case "ODD":
          varvalue = pos % 2 === 1;
          break;
        default:
          //console.log("Pos:", pos);
          varvalue = this.loop[varnames[0]].loop[pos][varnames[1]];
      }
    } else {
      varvalue = this.vm;
      varnames.forEach((v) => {
        if (varvalue instanceof Object && v in varvalue) {
          varvalue = v instanceof Object ? v : varvalue[v];
        } else {
          varvalue = undefined;
        }
      });
    }
    // console.log("Modifiers:", !!variable.modifiers, variable.modifiers);
    // console.log("Varvalue:", varvalue);
    return variable.modifiers
      ? this.modify_var(varvalue, variable.modifiers)
      : varvalue;
  }

  modify_var(varvalue, modifiers) {
    varvalue = typeof varvalue === "undefined" ? "" : varvalue;
    modifiers.forEach((modifier) => {
      let params = modifier.params.map((param) =>
        param.startsWith("$") ? this.get_var_value(param.substr(1)) : param
      );
      let method_name = `mod_${modifier.modifier.toUpperCase()}`;
      varvalue =
        method_name in this.tm && this.tm[method_name] instanceof Function
          ? this.tm[method_name](varvalue, params)
          : varvalue;
    });
    return varvalue;
  }

  eval_expr(expr) {
    let expression = [];
    let tokens = expr
      .split(" ")
      .map((token) => token.trim())
      .filter((token) => token.length);

    tokens.forEach((token) => {
      if (token.startsWith("$")) {
        token = `"${this.get_var_value(token.substr(1))}"`;
      }
      expression.push(token);
    });
    try {
      return eval(expression.join(""));
    } catch (e) {}
    return false;
  }

  // -- BLOCK METHODS
  cmd_IF(block) {
    let level = block[0][2];
    let tokens = [];
    let foundTrue = false;

    for (let i = 0; i < block.length; i++) {
      let token = block[i];
      if (token[0] === "IF" && token[2] === level) {
        foundTrue = this.eval_expr(token[1]);
      } else if (token[0] === "ELSEIF" && token[2] === level) {
        if (foundTrue) {
          break;
        } else {
          foundTrue = this.eval_expr(token[1]);
        }
      } else if (token[0] === "ELSE" && token[2] === level) {
        if (foundTrue) {
          break;
        } else {
          foundTrue = true;
        }
      } else if (token[0] === "ENDIF" && token[2] === level - 1) {
        break;
      } else if (foundTrue) {
        tokens.push(token);
      }
    }
    return this.parse_tokens(tokens);
  }

  cmd_FOREACH(block) {
    // console.log("foreach:", block);
    let params = { start: 0, step: 1, max: 0 };
    let result = [];
    let namespace;
    let loopvar;

    // console.log("block:", block);
    let cmd = block[0][1].split(" ").filter((item) => item.trim().length > 0);
    // console.log("params:", cmd);
    if (cmd[1].toUpperCase() === "IN") {
      namespace = cmd[0] && cmd[0].startsWith("$") ? cmd[0].substr(1) : null;
      loopvar = cmd[2] && cmd[2].startsWith("$") ? cmd[2].substr(1) : null;
    } else if (cmd[1].toUpperCase() === "AS") {
      loopvar = cmd[0] && cmd[0].startsWith("$") ? cmd[0].substr(1) : null;
      namespace = cmd[2] && cmd[2].startsWith("$") ? cmd[2].substr(1) : null;
    }
    if (!loopvar || !namespace) {
      throw `Error in loop: ${block[0][1]}`;
    }
    if (cmd.length > 3) {
      params = this.get_keys(block[0][1], params);
    }
    // console.log(namespace, loopvar, params);

    let varnames = loopvar.split(".");
    // console.log("fe varnames:", varnames);
    let loopList;
    if (Object.keys(this.loop).includes(varnames[0])) {
      let index = this.loop[varnames[0]]["pos"];
      loopList =
        (this.loop[varnames[0]] &&
          this.loop[varnames[0]]["loop"] &&
          this.loop[varnames[0]]["loop"][index] &&
          this.loop[varnames[0]]["loop"][index][varnames[1]]) ||
        null;
    } else {
      loopList = this.get_var_value(loopvar);
      // console.log("fe loopList:", loopList);
    }
    if (loopList && Array.isArray(loopList) && loopList.length) {
      let block_level = block[0][2];
      for (let i = 0; i < block.length; i++) {
        let token = block[i];
        if (
          (token[0] === "FOREACHELSE" && token[2] === block_level) ||
          (token[0] === "ENDFOREACH" && token[2] === block_level - 1)
        ) {
          block = block.slice(1, i);
          break;
        }
      }
      // console.log("loop block:", block);
      this.loop[namespace] = { loop: loopList };
      loopList.forEach((item, i) => {
        this.loop[namespace]["pos"] = i;
        result.push(this.parse_tokens(block));
      });
      delete this.loop[namespace];
    } else {
      block.every((token, i) => {
        // console.log(token, i);
        if (token[2] === block[0][2] && token[0] === "FOREACHELSE") {
          result.push(this.parse_tokens(block.slice(i + 1, -1)));
          return false;
        } else {
          return true;
        }
      });
    }
    return result.join("");
  }

  cmd_STRIP(block) {
    let text = this.parse_tokens(block.slice(1, -1));
    let patterns = [
      [/\s{2,}/g, ""],
      [/>\s/g, ">"],
      [/\s</g, "<"],
    ];
    return patterns.reduce((out, pattern) => {
      out = out.replace(pattern[0], pattern[1]);
      return out;
    }, text);
  }

  cmd_FILLSLOT(block) {
    let params = this.get_keys(block[0][1], { name: "" });
    if (params.name)
      this.slots[params.name] = this.parse_tokens(block.slice(1, -1));
  }

  // -- SINGLE TOKEN METHODS
  cmd_DEFINESLOT(token) {
    let params = this.get_keys(token, { name: "" });
    return params.name in this.slots ? this.slots[params.name] : "aaaa";
  }

  cmd_SET(token) {
    let params = this.get_keys(token, {}, true);
    Object.keys(params).forEach((key) => {
      this.set_var_value(key, this.eval_expr(params[key]));
    });
  }

  cmd_INCLUDE(token) {
    // console.log("Include:", token);
    let params = this.get_keys(token, { name: "", process: "yes" });
    // console.log("Include:", params);
    return [false, "no"].includes(params.process)
      ? this.fs.read(params.name)
      : this.render_file(params.name);
  }

  cmd_TXT(token) {
    // console.log("TXT token:", token, token.length);
    return token;
  }

  cmd_LDELIM(token) {
    return "{";
  }

  cmd_RDELIM(token) {
    return "}";
  }

  cmd_VAR(token) {
    return this.get_var_value(token);
  }
}

module.exports = {
  Neuron,
  render_file: function (file, data) {
    let n = new Neuron(data);
    return n.render_file(file);
  },
  render: function () {
    return this.render_file(...arguments);
  },
};
