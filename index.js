const fs = require("fs");

const neuron = require("./src/Neuron");
const data = require("./tests/data");

const parser = new neuron.Neuron(data);

res = parser.parse_file(process.argv[2]);

console.log(res);

fs.writeFileSync(process.argv[2].replace("templates", "results"), res);
